package com.test.githubexplorer.base;

import android.app.Application;
import android.content.Context;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.test.githubexplorer.BuildConfig;


/**
 * Created by yashthakur on 26/08/18.
 */

public class AppFile extends Application {


    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Logger.addLogAdapter(new AndroidLogAdapter());
        }


    }



    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

}
