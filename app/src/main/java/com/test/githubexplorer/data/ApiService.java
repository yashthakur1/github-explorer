package com.test.githubexplorer.data;

import com.test.githubexplorer.screens.models.ReposDataResponse;


import java.lang.reflect.Array;
import java.util.ArrayList;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by yashthakur on 26/08/18.
 */

public interface ApiService {

    @GET("/repositories")
    Observable<ArrayList<ReposDataResponse>> getReposData();

}
