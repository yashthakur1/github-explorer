package com.test.githubexplorer.screens;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.test.githubexplorer.R;
import com.test.githubexplorer.base.BaseActivity;

public class RepoDetailActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo_detail);
    }
}
