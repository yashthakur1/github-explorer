package com.test.githubexplorer.screens.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by yashthakur on 16/09/18.
 */

public class ReposDataResponse implements Parcelable {
    private String name;
    private String full_name;
    private String description;
    private String url;

    public ReposDataResponse(String name, String full_name, String description, String url) {
        this.name = name;
        this.full_name = full_name;
        this.description = description;
        this.url = url;
    }

    public ReposDataResponse() {
    }

    protected ReposDataResponse(Parcel in) {
        name = in.readString();
        full_name = in.readString();
        description = in.readString();
        url = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(full_name);
        dest.writeString(description);
        dest.writeString(url);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ReposDataResponse> CREATOR = new Creator<ReposDataResponse>() {
        @Override
        public ReposDataResponse createFromParcel(Parcel in) {
            return new ReposDataResponse(in);
        }

        @Override
        public ReposDataResponse[] newArray(int size) {
            return new ReposDataResponse[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
