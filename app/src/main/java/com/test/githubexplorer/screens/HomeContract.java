package com.test.githubexplorer.screens;


import com.test.githubexplorer.screens.models.ReposDataResponse;

import java.util.ArrayList;

/**
 * Created by yashthakur on 26/08/18.
 */

public interface HomeContract {

    interface View {
        void showRepos(ArrayList<ReposDataResponse> repos);

        void showError(String message);

        void showComplete();

        void showLoader();

        void hideLoader();


    }

    interface Presenter {
        void getCatalog();
    }
}

