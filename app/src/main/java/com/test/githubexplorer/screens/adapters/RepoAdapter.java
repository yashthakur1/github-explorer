package com.test.githubexplorer.screens.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;


import com.orhanobut.logger.Logger;
import com.squareup.picasso.Picasso;
import com.test.githubexplorer.R;
import com.test.githubexplorer.screens.models.ReposDataResponse;


import java.util.ArrayList;


/**
 * Created by yashthakur on 26/08/18.
 */

public class RepoAdapter extends RecyclerView.Adapter<RepoAdapter.UserViewHolder> {

    Context context;
    private ArrayList<ReposDataResponse> repos;
    public RepoAdapter.OnItemClickListener listener;

    public RepoAdapter(Context context, ArrayList<ReposDataResponse> repos) {
        this.context = context;
        this.repos = repos;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repo_bar, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserViewHolder viewHolder, final int position) {

        ReposDataResponse repoData = repos.get(position);
        String url = repoData.getUrl();

        Logger.d("BIND " + position);
        Picasso.get().load(url).into(viewHolder.imageView);
        viewHolder.btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(view, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return repos != null ? repos.size() : 0;
    }

    class UserViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        Button btnReject;

        UserViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image_view);
            btnReject = itemView.findViewById(R.id.btnReject);
        }
    }

    public void removeAt(int position) {
        repos.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, repos.size());
    }


    public interface OnItemClickListener {
        void onItemClick(View v, int position);
    }


}

