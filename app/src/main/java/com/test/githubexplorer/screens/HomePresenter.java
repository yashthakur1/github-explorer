package com.test.githubexplorer.screens;

import com.google.gson.Gson;
import com.orhanobut.logger.Logger;
import com.test.githubexplorer.data.ApiService;
import com.test.githubexplorer.screens.models.ReposDataResponse;


import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by yashthakur on 26/08/18.
 */

public class HomePresenter implements HomeContract.Presenter {

    Retrofit retrofit;
    HomeContract.View mView;

    @Inject
    public HomePresenter(Retrofit retrofit, HomeContract.View mView) {
        this.retrofit = retrofit;
        this.mView = mView;
    }


    @Override
    public void getCatalog() {
        mView.showLoader();
        retrofit.create(ApiService.class).getReposData().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<ArrayList<ReposDataResponse>>() {
                    @Override
                    public void onCompleted() {
                        mView.hideLoader();
                        mView.showComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(ArrayList<ReposDataResponse> reposData) {
                        Logger.json(new Gson().toJson(reposData));
                        mView.showRepos(reposData);
                    }
                });
    }
}
