package com.test.githubexplorer.screens;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.orhanobut.logger.Logger;
import com.test.githubexplorer.R;
import com.test.githubexplorer.base.BaseActivity;
import com.test.githubexplorer.screens.adapters.RepoAdapter;
import com.test.githubexplorer.screens.models.ReposDataResponse;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import jp.wasabeef.recyclerview.animators.SlideInRightAnimator;

public class HomeActivity extends BaseActivity implements HomeContract.View {

    @Inject
    HomePresenter homePresenter;

    @BindView(R.id.rvExplore)
    RecyclerView rvExplore;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        homePresenter.getCatalog();
    }


    @Override
    public void showRepos(ArrayList<ReposDataResponse> repos) {
        LinearLayoutManager linearLayoutManagerVertical = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        final RepoAdapter exploreAdapter = new RepoAdapter(context, repos);
        rvExplore.setItemAnimator(new SlideInRightAnimator());
        rvExplore.setLayoutManager(linearLayoutManagerVertical);

        rvExplore.setAdapter(exploreAdapter);
        exploreAdapter.listener = new RepoAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                exploreAdapter.removeAt(position);
            }
        };
    }

    @Override
    public void showError(String message) {
        Logger.d("API ERROR" + message);
    }

    @Override
    public void showComplete() {
        Logger.d("API complete");
    }

    @Override
    public void showLoader() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        progressBar.setVisibility(View.GONE);
    }
}
